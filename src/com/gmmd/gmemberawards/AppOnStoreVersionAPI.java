package com.gmmd.gmemberawards;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;
 
public class AppOnStoreVersionAPI {
 
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    public static AppOnStoreVersionParseJson  Rgal = new AppOnStoreVersionParseJson();
    
    public static AppOnStoreVersion getJSONFromUrl() {
 
    	BufferedReader in = null;
		String result = null;
		
    	if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
    	try {
			String url = "http://thestar9.gmmwireless.com:80/thestar9/api_awards/index.php/app_on_store/get?" +
					"APP_ID=2&APPVERSION=1";
	
			Log.i("","url AppOnStoreVersion : "+url);
	
			HttpClient client = new DefaultHttpClient();
	
			HttpConnectionParams.setConnectionTimeout(client.getParams(),3000);
			HttpConnectionParams.setSoTimeout(client.getParams(),3000);
	
			HttpGet request = new HttpGet(url);
			request.setHeader("User-Agent", "Android " + android.os.Build.VERSION.RELEASE + "/" + android.os.Build.MODEL);
	
			HttpResponse response = client.execute(request);
	
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
	
			result = sb.toString();
			
			Log.i("","result AppOnStoreVersion : "+result);
		}catch(Exception e){
		
	
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return Rgal.setvalueInclass(result);
    }
}