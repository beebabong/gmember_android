package com.gmmd.gmemberawards;

import com.gmmd.gmemberawards.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


public class Webview_gmember extends Activity{
	public static String service_ID;
	public static String gmmdCode;
	ProgressBar progress ;
	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		
		
		String link = getIntent().getStringExtra("link");
		
		Log.i("","link"+link);
		
		WebView mWebView = (WebView) findViewById(R.id.webView1);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        mWebView.setBackgroundColor(Color.TRANSPARENT);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_INSET);
//        JavaScriptHandler jsHandle = new JavaScriptHandler();
//        jsHandle.setActivity(this);
////        mWebView.addJavascriptInterface(jsHandle, "agreement");
//        mWebView.addJavascriptInterface(jsHandle, "MyHandler");
//        
        mWebView.loadUrl(link);
//        mWebView.setWebViewClient(new MyWebViewClient());
//       
//        
//        runOnUiThread(new Runnable() {
//            public void run() {
//              
//            }
//        });
	}
	
//	
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if(keyCode == KeyEvent.KEYCODE_BACK){
//			finish();
//		}
//		return false;
//	}
//	
	
	
	
	private class MyWebViewClient extends WebViewClient { 
         
        public boolean shouldOverrideUrlLoading(WebView view, String url) { 
            view.loadUrl (url); 
            return true;
        } 
        
        
        public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        }
    }
	
	
}
