package com.gmmd.gmemberawards;

import org.json.JSONException;
import org.json.JSONObject;

public class AppOnStoreVersionParseJson {
	
	String STATUS_CODE="STATUS_CODE";
	String STATUS_DETAIL ="STATUS_DETAIL";
	String appID ="appID";
	String appOnStoreVersion ="appOnStoreVersion";
	String appMustUpdate ="appMustUpdate";
	String appMessage ="appMessage";
	String storelink ="storelink";
	String fake_apple ="fake_apple";
	
	AppOnStoreVersion rr = new AppOnStoreVersion();
	@SuppressWarnings("static-access")
	public AppOnStoreVersion setvalueInclass(String strJSON){
		try {
			
            JSONObject jObject = new JSONObject(strJSON);
            
            String a_statusCode = jObject.getString(STATUS_CODE);
            String a_detail = jObject.getString(STATUS_DETAIL);
            String a_appID = jObject.getString(appID);
            String a_appOnStoreVersion = jObject.getString(appOnStoreVersion);
            String a_appMustUpdate = jObject.getString(appMustUpdate);
            String a_appMessage = jObject.getString(appMessage);
            String a_storelink = jObject.getString(storelink);
           
            rr.setSTATUS_CODE(a_statusCode);
            rr.setSTATUS_DETAIL(a_detail);
            rr.setAppID(a_appID);
            rr.setAppOnStoreVersion(a_appOnStoreVersion);
            rr.setAppMustUpdate(a_appMustUpdate);
            rr.setAppMessage(a_appMessage);
            rr.setStorelink(a_storelink);
           
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return rr;
	}
	
}
