package com.gmmd.gmemberawards;


import com.gmmd.gmemberawards.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Dialog_Alert extends Dialog implements android.view.View.OnClickListener{

	Button btn_submit;
	Context c;
	String message;
	
	public Dialog_Alert(Context context,String message) {
		super(context);
		this.message = message;
		c= context;
	}

	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 this.setContentView(R.layout.showdialog_alert); 
		 btn_submit = (Button) findViewById(R.id.btn_submit);
//		 txt_vote = (TextView) findViewById(R.id.txt_vote);
		 
		 this.setTitle(message);
//		 txt_vote.setti(message);
		 btn_submit.setOnClickListener(this);
	}

	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_submit:
			this.dismiss();
			break;
	
		}
	}

	
}
