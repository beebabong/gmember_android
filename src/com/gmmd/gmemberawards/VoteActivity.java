package com.gmmd.gmemberawards;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.gmmd.gmemberawards.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class VoteActivity  extends Activity{
	
	public static int listStar;
	
	TextView tvTimer1;
	TextView tvTimer2;
	TextView tvTimer3;
	TextView tvTimer4;
	
	ListView lisView;
	
	ImageView img_listmenu,lastpress;
	int pos =0;
	
	long diff;
	long endTimeMillSec;
	long nowTimeMilliSec;
	
	private MyListViewAdapter myListViewAdapter;
	private Context context;

	String DATE_VOTE_START,DATE_VOTE_END,DATE_WINNER;
	
	int[] arrImg = {R.drawable.man,
			 R.drawable.girl,
			 R.drawable.band,
			 R.drawable.artistrock,
			 R.drawable.onfocus,
			 R.drawable.artist_looktung_m,
			 R.drawable.artist_looktung_w,
			 R.drawable.musichit,
			 R.drawable.rock,
			 R.drawable.looktung,
			 R.drawable.movie,
			 R.drawable.tv,
			 R.drawable.mv,
			 R.drawable.entertainer,
			 R.drawable.bestcouple
			 };
	
	int[] arrImg_active = {R.drawable.man_a,
			 R.drawable.girl_a,
			 R.drawable.band_a,
			 R.drawable.artistrock_a,
			 R.drawable.onfocus_a,
			 R.drawable.artist_looktung_m_a,
			 R.drawable.artist_looktung_w_a,
			 R.drawable.musichit_a,
			 R.drawable.rock_a,
			 R.drawable.looktung_a,
			 R.drawable.movie_a,
			 R.drawable.tv_a,
			 R.drawable.mv_a,
			 R.drawable.entertainer_a,
			 R.drawable.bestcouple_a
			 };

	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vote);
		
		context = this;
		lisView = (ListView)findViewById(R.id.listView1);


// Time-----------------------------------------------------------------------------------------------		
		tvTimer1 = (TextView) findViewById(R.id.textView1);
		tvTimer2 = (TextView) findViewById(R.id.textView2);
		tvTimer3 = (TextView) findViewById(R.id.textView3);
		tvTimer4 = (TextView) findViewById(R.id.textView4);
		img_listmenu = (ImageView) findViewById(R.id.img_listmenu);
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
		formatter.setLenient(false);

		Calendar c = Calendar.getInstance(); 
		Log.i("year", "year"+c.get(Calendar.YEAR));
		if(c.get(Calendar.YEAR)==2014) {
			DATE_VOTE_START = getString(R.string.DateVoteStart);
			DATE_VOTE_END= getString(R.string.DateVoteEnd);
			DATE_WINNER= getString(R.string.DateWinnerStart);
		}else{
			DATE_VOTE_START = getString(R.string.DateVoteStart_th);
			DATE_VOTE_END= getString(R.string.DateVoteEnd_th);
			DATE_WINNER= getString(R.string.DateWinnerStart_th);
			
		}
		
		String oldTime = DATE_VOTE_END;
		Date oldDate;
		img_listmenu.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		lisView.setAdapter(new MyListViewAdapter());
		try {
			oldDate = formatter.parse(oldTime);
			endTimeMillSec = oldDate.getTime();
			nowTimeMilliSec = System.currentTimeMillis();

			diff = endTimeMillSec - nowTimeMilliSec;

			// set start text
			setDateText(diff);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new Thread() {
			public void run() {
				while (diff >= 0) {
					try {
						Thread.sleep(1000);
						diff -= 1000;
						// set text
						setDateText(diff);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
		}.start();

	}//end onCreate
	
	
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		

		
	}
	


	 private class MyListViewAdapter extends BaseAdapter{
			
			public ViewHolder viewHolder;
			
			public int getCount() {
				return 15;
			}

			
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@SuppressWarnings("unchecked")
			
			public View getView(final int position, View view, ViewGroup parent) {
				//Create
				if(view==null){
					view = LayoutInflater.from(context).inflate(R.layout.layout_vote, null);
					viewHolder = new ViewHolder();
					viewHolder.title = (TextView) view.findViewById(R.id.textView1);
					viewHolder.imgs = (ImageView) view.findViewById(R.id.imageView1);
					viewHolder.layout_main = (LinearLayout) view.findViewById(R.id.layout_main);
//					
					view.setTag(viewHolder);
				}else{
					viewHolder = (ViewHolder) view.getTag();
					
				}

				viewHolder.title.setText(MainMenuActivity.MyListStar.get(position).getTitle().toString());
					
				viewHolder.imgs.setImageResource(arrImg[position]);
				viewHolder.layout_main.setBackgroundResource(R.color.white);
					
				viewHolder.title.setOnClickListener(new OnClickListener() {
						
						
						public void onClick(View v) {
							ImageView imageview = (ImageView) ((View) v.getParent()).findViewById(R.id.imageView1);
							imageview.setImageResource(arrImg_active[position]);
							
							if(lastpress!=null){
								if(lastpress!=imageview) lastpress.setImageResource(arrImg[pos]);
							}
							lastpress = imageview;
							pos = position;
//							imageview.isClickable();
//							imageview.setClickable(true);
							if(position!=14){
								Intent i = new Intent(VoteActivity.this, VoteDetailActivity.class)
								.putExtra("position", position);
							    startActivity(i);
							}else{
								Intent i = new Intent(VoteActivity.this, VoteLoverActivity.class);
							    startActivity(i);
							}
							
							
						}
					});
//			
				return view;
				
			}
			

			private class ViewHolder{
				
				public TextView title;
				public ImageView imgs;
				public LinearLayout layout_main;
			}
			
			
		}
	 
	 //==================================================================================
	 

	  //End List View----------------------------------------------------------------------
	
	private void setDateText(final long millisec) {

		runOnUiThread(new Runnable() {
			public void run() {
				// TODO Auto-generated method stub
//				Log.e("day", "miliday" + millisec);

				long seconds = (long) (millisec / 1000) % 60;
//				Log.e("secnd", "miliday" + seconds);

				long minutes = (long) ((millisec / (1000 * 60)) % 60);
//				Log.e("minute", "miliday" + minutes);

				long hours = (long) ((millisec / (1000 * 60 * 60)) % 24);
//				Log.e("hour", "miliday" + hours);

				long days = (int) ((millisec / (1000 * 60 * 60 * 24)) % 365);
//				Log.e("days", "miliday" + days);

				 String serverUptimeText1 = String.format("%02d",days);
				 String serverUptimeText2 = String.format("%02d",hours);
				 String serverUptimeText3 = String.format("%02d",minutes);
				 String serverUptimeText4 = String.format("%02d",seconds); 
				
				
				 tvTimer1.setText(serverUptimeText1);
				 tvTimer2.setText(serverUptimeText2);
				 tvTimer3.setText(serverUptimeText3);
				 tvTimer4.setText(serverUptimeText4);
			}
		});

	}
	
	//----------------------------------------------------------------------------------------
	

	
}

