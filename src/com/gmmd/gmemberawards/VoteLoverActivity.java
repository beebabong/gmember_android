package com.gmmd.gmemberawards;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.gmmd.gmemberawards.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class VoteLoverActivity extends Activity{
	
	TextView tvTime1;
	TextView tvTime2;
	TextView tvTime3;
	TextView tvTime4;
	ListView listview;
	TextView txt_title;
	
	long diff;
	long endTimeMillSec;
	long nowTimeMilliSec;
	int position_title,width;

	String DATE_VOTE_START,DATE_VOTE_END,DATE_WINNER;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vote_lover_detail);
		
		width = getWindowManager().getDefaultDisplay().getWidth();
		
		
		txt_title = (TextView) findViewById(R.id.txt_title);
		txt_title.setText(MainMenuActivity.MyListStar.get(14).getTitle().toString());
		
		listview = (ListView) findViewById(R.id.listview);
		listview.setAdapter(new ImageAdapter(this));
		listview.setOnItemClickListener(new OnItemClickListener() {

			
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String message = "คุณต้องการโหวตให้กับ "+MainMenuActivity.MyListStar.get(14).getList_name().get(pos).getName()+
						" ในสาขา " + MainMenuActivity.MyListStar.get(14).getTitle();
				
				new Dialog_Vote(VoteLoverActivity.this, MainMenuActivity.MyListStar.get(14).getList_name().get(pos).getNumber().toString(), message).show();
			}
		});
		
		//Button Vote
		final ImageView buttonVote = (ImageView) findViewById(R.id.img_btnvoteresult);
			buttonVote.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
		        Intent i = new Intent(VoteLoverActivity.this, VoteResultActivity.class);
				startActivity(i);
		    }
		 });
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------	
			
		for(list_star s:MainMenuActivity.MyListStar){
			Log.i("", "string"+s.getTitle() );
			for(name_star ns : s.getList_name()) {
				MainMenuActivity.MyListStar.get(0).getList_name().get(0).getName();
				Log.i("", "string name : "+ns.getNumber() +"//"+ ns.getName());
			}
		}
		
//---------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------
		tvTime1 = (TextView) findViewById(R.id.textView1);
		tvTime2 = (TextView) findViewById(R.id.textView2);
		tvTime3 = (TextView) findViewById(R.id.textView3);
		tvTime4 = (TextView) findViewById(R.id.textView4);

		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
		formatter.setLenient(false);

		Calendar c = Calendar.getInstance(); 
		Log.i("year", "year"+c.get(Calendar.YEAR));
		if(c.get(Calendar.YEAR)==2014) {
			DATE_VOTE_START = getString(R.string.DateVoteStart);
			DATE_VOTE_END= getString(R.string.DateVoteEnd);
			DATE_WINNER= getString(R.string.DateWinnerStart);
		}else{
			DATE_VOTE_START = getString(R.string.DateVoteStart_th);
			DATE_VOTE_END= getString(R.string.DateVoteEnd_th);
			DATE_WINNER= getString(R.string.DateWinnerStart_th);
			
		}
		
		String oldTime = DATE_VOTE_END;
		Date oldDate;

		try {
			oldDate = formatter.parse(oldTime);
			endTimeMillSec = oldDate.getTime();
			nowTimeMilliSec = System.currentTimeMillis();

			diff = endTimeMillSec - nowTimeMilliSec;

			// set start text
			setDateText(diff);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new Thread() {
			public void run() {
				while (diff >= 0) {
					try {
						Thread.sleep(1000);
						diff -= 1000;
						// set text
						setDateText(diff);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
		}.start();

	}


	private void setDateText(final long millisec) {

		runOnUiThread(new Runnable() {
			public void run() {
				long seconds = (long) (millisec / 1000) % 60;

				long minutes = (long) ((millisec / (1000 * 60)) % 60);

				long hours = (long) ((millisec / (1000 * 60 * 60)) % 24);

				long days = (int) ((millisec / (1000 * 60 * 60 * 24)) % 365);
				
				
				 String serverUptimeText1 = String.format("%02d",days);
				 String serverUptimeText2 = String.format("%02d",hours);
				 String serverUptimeText3 = String.format("%02d",minutes);
				 String serverUptimeText4 = String.format("%02d",seconds); 
				 
				 if(millisec>0){
					 tvTime1.setText(serverUptimeText1);
					 tvTime2.setText(serverUptimeText2);
					 tvTime3.setText(serverUptimeText3);
					 tvTime4.setText(serverUptimeText4);
				}else{
					 tvTime1.setText("00");
					 tvTime2.setText("00");
					 tvTime3.setText("00");
					 tvTime4.setText("00");
				}
				
			}
		});

	}
	
	class ImageAdapter extends BaseAdapter {
        private Context mcontext;
        public ImageAdapter(Context c)
        {
        	mcontext=c;
        }

        
        public int getCount() {
        	return MainMenuActivity.MyListStar.get(14).getList_name().size();
        }

        
        public Object getItem(int position) {
        	return null;
        }

        
        public long getItemId(int position) {
        	return 0;
        }

        
        public View getView(int pos, View convertView, ViewGroup parent) {
        
        	ImageView imageView = new ImageView(mcontext);
          imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
          GridView.LayoutParams flpgal = new GridView.LayoutParams(width,width/2);
          imageView.setLayoutParams(flpgal);
         
          imageView.setImageResource(MainMenuActivity.MyListStar.get(14).getList_name().get(pos).getResource_image());
            return imageView;
        }


}
	
}
