package com.gmmd.gmemberawards;

import java.util.ArrayList;
import java.util.List;

public class list_star {

	String title;
	List<name_star> list_name = new ArrayList<name_star>();
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<name_star> getList_name() {
		return list_name;
	}
	public void setList_name(List<name_star> list_name) {
		this.list_name = list_name;
	}
	

}

class name_star {

	String number;
	String name;
	int resource_image;
	int resource_image_resultvote;
	
	public int getResource_image_resultvote() {
		return resource_image_resultvote;
	}
	public void setResource_image_resultvote(int resource_image_resultvote) {
		this.resource_image_resultvote = resource_image_resultvote;
	}
	
	public int getResource_image() {
		return resource_image;
	}
	public void setResource_image(int resource_image) {
		this.resource_image = resource_image;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}