package com.gmmd.gmemberawards;


import com.gmmd.gmemberawards.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;

import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class VoteResultActivity extends Activity{
	
	private MyListViewAdapter myListViewAdapter;
	private Context context;
	ListView listView1;
//	private static final String[] COUNTRIES = new String[] {
//		"1.January - Jan", "2.February - Feb", "3.March - Mar", "4.April - Apr", "5.May - May", "6.June - Jun"
//		, "7.July - Jul", "8.August - Aug", "9.September - Sep", "10.October - Oct", "11.November - Nov", "12.December - Dec"
//	};
	ImageView lastpress;
	int pos =0;
	int[] arrImg = {R.drawable.man,
			 R.drawable.girl,
			 R.drawable.band,
			 R.drawable.artistrock,
			 R.drawable.onfocus,
			 R.drawable.artist_looktung_m,
			 R.drawable.artist_looktung_w,
			 R.drawable.musichit,
			 R.drawable.rock,
			 R.drawable.looktung,
			 R.drawable.movie,
			 R.drawable.tv,
			 R.drawable.mv,
			 R.drawable.entertainer,
			 R.drawable.bestcouple
			 };
	
	int[] arrImg_active = {R.drawable.man_a,
			 R.drawable.girl_a,
			 R.drawable.band_a,
			 R.drawable.artistrock_a,
			 R.drawable.onfocus_a,
			 R.drawable.artist_looktung_m_a,
			 R.drawable.artist_looktung_w_a,
			 R.drawable.musichit_a,
			 R.drawable.rock_a,
			 R.drawable.looktung_a,
			 R.drawable.movie_a,
			 R.drawable.tv_a,
			 R.drawable.mv_a,
			 R.drawable.entertainer_a,
			 R.drawable.bestcouple_a
			 };
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vote_result);
		
		context = this;
		listView1 = (ListView)findViewById(R.id.listView1);

		
		myListViewAdapter = new MyListViewAdapter();
		
		listView1.setAdapter(myListViewAdapter);
		listView1.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?>parent,View view,int postition,long id){
						
//				ShowShopActivity.dataShop = data.get(postition);
				Intent i = new Intent(VoteResultActivity.this, VoteResultDetailActivity.class);
			    startActivity(i);
			              

			}
		});
		
	}
	
	
	 private class MyListViewAdapter extends BaseAdapter{
			
			public ViewHolder viewHolder;
			
			public int getCount() {
				return 15;
			}

			
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@SuppressWarnings("unchecked")
			
			public View getView(final int position, View view, ViewGroup parent) {
				//Create
				if(view==null){
					view = LayoutInflater.from(context).inflate(R.layout.layout_vote, null);
					viewHolder = new ViewHolder();
					viewHolder.title = (TextView) view.findViewById(R.id.textView1);
					viewHolder.imgs = (ImageView) view.findViewById(R.id.imageView1);
					viewHolder.layout_main = (LinearLayout) view.findViewById(R.id.layout_main);
//					
					view.setTag(viewHolder);
				}else{
					viewHolder = (ViewHolder) view.getTag();
					
				}

				viewHolder.title.setText(MainMenuActivity.MyListStar.get(position).getTitle().toString());
					
				viewHolder.imgs.setImageResource(arrImg[position]);
				viewHolder.layout_main.setBackgroundResource(R.color.white);
					
				viewHolder.title.setOnClickListener(new OnClickListener() {
						
						
						public void onClick(View v) {
							ImageView imageview = (ImageView) ((View) v.getParent()).findViewById(R.id.imageView1);
							imageview.setImageResource(arrImg_active[position]);
							
							if(lastpress!=null){
								if(lastpress!=imageview) lastpress.setImageResource(arrImg[pos]);
							}
							lastpress = imageview;
							pos = position;
							
							int lastindex=MainMenuActivity.MyListStar.get(position).getList_name().size();
							String range_category = MainMenuActivity.MyListStar.get(position).getList_name().get(0).getNumber()+"-"+
									 MainMenuActivity.MyListStar.get(position).getList_name().get(lastindex-1).getNumber();
							
								Intent i = new Intent(VoteResultActivity.this, VoteResultDetailActivity.class)
								.putExtra("position", position).putExtra("range_category", range_category);
							    startActivity(i);
							
							
							
						}
					});
//			
				return view;
				
			}
			

			private class ViewHolder{
				
				public TextView title;
				public ImageView imgs;
				public LinearLayout layout_main;
			}
	 }

	  //End List View----------------------------------------------------------------------
}
