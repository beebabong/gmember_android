package com.gmmd.gmemberawards;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.LinearGradient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gmmd.gmemberawards.R;
import com.gmmd.gmemberawards.VoteResultDetailActivity.AsyncTaskParseJson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class WinnerActivity extends Activity {

	DisplayImageOptions options;
	ImageView[] Img_person_winner = new ImageView[15];
	ImageView img_listmenu;
	int position_title, width, position;
	List<GmemberData> gmemberDataList = new ArrayList<GmemberData>();
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	@SuppressWarnings("deprecation")
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_winner);
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.blank)
		.showImageForEmptyUri(R.drawable.blank)
		.showImageOnFail(R.drawable.blank)
		.cacheInMemory().cacheOnDisc()
		.bitmapConfig(Bitmap.Config.RGB_565).build();
		imageLoader.init(ImageLoaderConfiguration
		.createDefault(WinnerActivity.this));
		
		position = getIntent().getIntExtra("position", 0);
		
		if(DeviceInfo.isCheckInternet(this)) new AsyncTaskParseJson().execute();
		else new Dialog_Alert(WinnerActivity.this, "Sorry, Not Internet Connection.").show();
		

		position_title = getIntent().getIntExtra("position", 0);
		width = getWindowManager().getDefaultDisplay().getWidth();
		
		for(int i=1;i<=15;++i){
			Img_person_winner[i-1] = (ImageView) findViewById(getResources().getIdentifier("imageView"+i, "id", getPackageName()));
		}
		img_listmenu = (ImageView) findViewById(R.id.img_listmenu);


		img_listmenu.setOnClickListener(new OnClickListener() {
			
			
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

		final String TAG = "AsyncTaskParseJson.java";

		String getAwardUrl = "http://thestar9.gmmwireless.com:80/thestar9/api_awards/index.php/awards/get";

		// contacts JSONArray
		JSONArray dataJsonArr = null;

		
		protected void onPreExecute() {
		}

		
		protected String doInBackground(String... arg0) {
			try {
				// instantiate our json parser
				JsonParser jParser = new JsonParser();

				// get json string from url
				JSONObject json = jParser.getJSONFromUrl(getAwardUrl);

				// get status and detail
				String status = json.getString("STATUS");
				String detail = json.getString("DETAIL");
				String version = json.getString("VERSION_CONTROL");

				// get the array of data
				dataJsonArr = json.getJSONArray("DATA");
				// loop through all data
				if (status.equals("200")) {
					for (int i = 0; i < dataJsonArr.length(); i++) {
						GmemberData gmemberData = new GmemberData();
						JSONObject datalistVote = dataJsonArr.getJSONObject(i);

						// Storing each json item in variable
						String url_gmember = datalistVote
								.getString("URL_GMEMBER");
						String url_image = datalistVote.getString("URL_IMAGE");
						String category_id = datalistVote
								.getString("CATEGORY_ID");
						String category_name = datalistVote
								.getString("CATEGORY_NAME");

						// add data (string) to votelistdata
						gmemberData.setURL_GMEMBER(url_gmember);
						gmemberData.setURL_IMAGE(url_image);
						gmemberData.setCATEGORY_ID(category_id);
						gmemberData.setCATEGORY_NAME(category_name);

						// add data (Object) to list_voteData
						gmemberDataList.add(gmemberData);
					}

				} else {
					Log.i("", "Detail >> " + detail);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		
		protected void onPostExecute(String strFromDoInBg) {
			
			
			
			for(int i=0;i<14;++i){
				final int pos_img;
				Img_person_winner[i].setScaleType(ImageView.ScaleType.FIT_CENTER);
				LinearLayout.LayoutParams flpgal = new LinearLayout.LayoutParams(width / 2,
						width / 2);
				flpgal.setMargins(2, 2, 2, 2);
				Img_person_winner[i].setLayoutParams(flpgal);
				
				imageLoader.displayImage(gmemberDataList.get(i).getURL_IMAGE(),
						Img_person_winner[i], options);
				
				pos_img = i;
				Img_person_winner[i].setOnClickListener(new OnClickListener() {
					
					
					public void onClick(View v) {
						startActivity(new Intent(getApplicationContext(),Webview_gmember.class).putExtra("link", gmemberDataList.get(pos_img).getURL_GMEMBER()));
					}
				});
			}
			
			Img_person_winner[14].setScaleType(ImageView.ScaleType.FIT_CENTER);
			LinearLayout.LayoutParams flpgal = new LinearLayout.LayoutParams(width,
					width / 2);
			flpgal.setMargins(2, 2, 2, 2);
			Img_person_winner[14].setLayoutParams(flpgal);
			
			imageLoader.displayImage(gmemberDataList.get(14).getURL_IMAGE(),
					Img_person_winner[14], options);
			
			
			Img_person_winner[14].setOnClickListener(new OnClickListener() {
				
				
				public void onClick(View v) {
					startActivity(new Intent(getApplicationContext(),Webview_gmember.class).putExtra("link", gmemberDataList.get(14).getURL_GMEMBER()));
				}
			});
		}

	}

	
}
