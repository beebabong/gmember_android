package com.gmmd.gmemberawards;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.util.Log;

public final class DeviceInfo {
	
	// check internet
	public static boolean isCheckInternet(Context con) {
		ConnectivityManager connec = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
				|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
			// connect
			return true;
		} else {
			// not-connect
			return false;
		}
	}
	
	public static String getConnectionType( Context context) {
		Log.i("","--getConnectionType");

		String connectionType = "";

		ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		State mobile = conMan.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState();
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
				.getState();

		if (mobile == NetworkInfo.State.CONNECTED
				|| mobile == NetworkInfo.State.CONNECTING) {
			connectionType = "3G";
		}

		if (wifi == NetworkInfo.State.CONNECTED
				|| wifi == NetworkInfo.State.CONNECTING) {
			connectionType = "WIFI";
		}

		Log.i("","== connection : " + connectionType);
		return connectionType;
	}
}
