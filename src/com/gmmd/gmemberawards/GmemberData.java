package com.gmmd.gmemberawards;

public class GmemberData {
	
	String URL_GMEMBER;
	String URL_IMAGE;
	String CATEGORY_ID;
	String CATEGORY_NAME;
	
	public String getURL_GMEMBER() {
		return URL_GMEMBER;
	}
	public void setURL_GMEMBER(String uRL_GMEMBER) {
		URL_GMEMBER = uRL_GMEMBER;
	}
	public String getURL_IMAGE() {
		return URL_IMAGE;
	}
	public void setURL_IMAGE(String uRL_IMAGE) {
		URL_IMAGE = uRL_IMAGE;
	}
	public String getCATEGORY_ID() {
		return CATEGORY_ID;
	}
	public void setCATEGORY_ID(String cATEGORY_ID) {
		CATEGORY_ID = cATEGORY_ID;
	}
	public String getCATEGORY_NAME() {
		return CATEGORY_NAME;
	}
	public void setCATEGORY_NAME(String cATEGORY_NAME) {
		CATEGORY_NAME = cATEGORY_NAME;
	}
	

}
