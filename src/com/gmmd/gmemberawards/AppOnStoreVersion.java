package com.gmmd.gmemberawards;

public class AppOnStoreVersion {

	String STATUS_CODE;
	String STATUS_DETAIL ;
	String appID ;
	String appOnStoreVersion ;
	String appMustUpdate ;
	String appMessage ;
	String storelink ;
	String fake_apple ;
	
	public String getSTATUS_CODE() {
		return STATUS_CODE;
	}
	public void setSTATUS_CODE(String sTATUS_CODE) {
		STATUS_CODE = sTATUS_CODE;
	}
	public String getSTATUS_DETAIL() {
		return STATUS_DETAIL;
	}
	public void setSTATUS_DETAIL(String sTATUS_DETAIL) {
		STATUS_DETAIL = sTATUS_DETAIL;
	}
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getAppOnStoreVersion() {
		return appOnStoreVersion;
	}
	public void setAppOnStoreVersion(String appOnStoreVersion) {
		this.appOnStoreVersion = appOnStoreVersion;
	}
	public String getAppMustUpdate() {
		return appMustUpdate;
	}
	public void setAppMustUpdate(String appMustUpdate) {
		this.appMustUpdate = appMustUpdate;
	}
	public String getAppMessage() {
		return appMessage;
	}
	public void setAppMessage(String appMessage) {
		this.appMessage = appMessage;
	}
	public String getStorelink() {
		return storelink;
	}
	public void setStorelink(String storelink) {
		this.storelink = storelink;
	}
	public String getFake_apple() {
		return fake_apple;
	}
	public void setFake_apple(String fake_apple) {
		this.fake_apple = fake_apple;
	}
	
	
	
}
