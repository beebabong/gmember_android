package com.gmmd.gmemberawards;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainMenuActivity extends Activity {
	public String gg,t_data,t_status;
	public static String versioncontrol = "1";
	ImageView img;
	AppOnStoreVersion appversion;
	
	TextView tvTime1;
	TextView tvTime2;
	TextView tvTime3;
	TextView tvTime4;
	
	long diff,diff_th;
	long endTimeMillSec;
	long nowTimeMilliSec;
	protected Object list_star;
	SimpleDateFormat formatter;
	public static List<list_star> MyListStar = new ArrayList<list_star>();
	
	int BEFORE_VOTE = 0, DURING_VOTE=1, AFTER_VOTE=2;
	int DECLARE = 0, NO_DECLARE=1;

	String DATE_VOTE_START,DATE_VOTE_END,DATE_WINNER;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
		img = (ImageView)findViewById(R.id.imageView1);
		
		if(DeviceInfo.isCheckInternet(this)){
			if(checkversion() ) {
				if(appversion.getAppMustUpdate().toLowerCase().equals("no")) Alert_update();
	          	else Force_update();
			
			}else{
				startmenu();
			}
		}else{
			startmenu();
		}
		
			
		
	}
	
	void startmenu(){
		if(DeviceInfo.isCheckInternet(this)) new AsyncTaskParseJson().execute();
		
		Getdata_List();
		//Button Vote
		final ImageView buttonVote = (ImageView) findViewById(R.id.imageBtnVote);
		buttonVote.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	
            	if(checkDateVote() == DURING_VOTE){
            		Intent i = new Intent(MainMenuActivity.this, VoteActivity.class);
            		startActivity(i);
            	}
            	else if(checkDateVote() == AFTER_VOTE)Toast.makeText(getApplicationContext(), "หมดเวลาโหวตแล้วคะ", Toast.LENGTH_SHORT).show();
            	else if(checkDateVote() == BEFORE_VOTE)Toast.makeText(getApplicationContext(), "ยังไม่ถึงเวลาโหวตคะ", Toast.LENGTH_SHORT).show();
            }


        });
		
		//Button Results
		final ImageView buttonWin = (ImageView) findViewById(R.id.imageBtnResults);
		buttonWin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(checkDateDeclare()==DECLARE){
            		Intent ii = new Intent(MainMenuActivity.this, WinnerActivity.class);
    			    startActivity(ii) ;
            	}else {
            		Intent ii = new Intent(MainMenuActivity.this, HowToActivity.class).putExtra("comingsoon", true);
    			    startActivity(ii) ;
            		
            	}
            	
            }
        });
		
		//Button How To
		final ImageView buttonHowTo = (ImageView) findViewById(R.id.imageBtnHowto);
		buttonHowTo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent iii = new Intent(MainMenuActivity.this, HowToActivity.class);
			    startActivity(iii);
            }
        });
		
		tvTime1 = (TextView) findViewById(R.id.textView1);
		tvTime2 = (TextView) findViewById(R.id.textView2);
		tvTime3 = (TextView) findViewById(R.id.textView3);
		tvTime4 = (TextView) findViewById(R.id.textView4);

		formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
		formatter.setLenient(false);
		Calendar c = Calendar.getInstance(); 
		Log.i("year", "year"+c.get(Calendar.YEAR));
		if(c.get(Calendar.YEAR)==2014) {
			DATE_VOTE_START = getString(R.string.DateVoteStart);
			DATE_VOTE_END= getString(R.string.DateVoteEnd);
			DATE_WINNER= getString(R.string.DateWinnerStart);
		}else{
			DATE_VOTE_START = getString(R.string.DateVoteStart_th);
			DATE_VOTE_END= getString(R.string.DateVoteEnd_th);
			DATE_WINNER= getString(R.string.DateWinnerStart_th);
			
		}
		
		String oldTime = DATE_VOTE_END;
		Date oldDate;

		try {
			oldDate = formatter.parse(oldTime);
			
			endTimeMillSec = oldDate.getTime();
			nowTimeMilliSec = System.currentTimeMillis();
			
			diff = endTimeMillSec - nowTimeMilliSec;
					

			// set start text
			setDateText(diff);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new Thread() {
			public void run() {
				while (diff >= 0) {
					try {
						Thread.sleep(1000);
						diff -= 1000;
						// set text
						setDateText(diff);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};
		}.start();

	
	}

	public void Alert_update(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Update Version");
	
		builder.setMessage("Do you want update ??")
		       .setCancelable(true)
		       
		       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.gmmd.gmemberawards"));
					    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					    startActivity(i);	
		           }
		       })
		       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   startmenu();
		           }
		       });
		 builder.show();
		 
		
	}
	
	 public void Force_update(){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Update Version");
		
			builder.setMessage("you must update this application before.")
			       .setCancelable(true)
			       .setNeutralButton("OK", new OnClickListener() {
					
					public void onClick(DialogInterface arg0, int arg1) {
						try {
							
						    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.gmmd.gmemberawards"));
						    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						    startActivity(i);
						    Intent startMain = new Intent(
									Intent.ACTION_MAIN);
							startMain.addCategory(Intent.CATEGORY_HOME);
							startMain
									.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(startMain);
							onDestroy();
							android.os.Process
									.killProcess(android.os.Process.myPid());
						    
						} catch (android.content.ActivityNotFoundException anfe) {
						 
						}
					}
				});
			       
			 builder.show();
			 
		}
 
	
	private boolean checkversion() {
		appversion = AppOnStoreVersionAPI.getJSONFromUrl();
		try {
			Log.i("", "version : "+appversion.getAppOnStoreVersion()+"//"+String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode));
		
			if(Integer.valueOf(appversion.getAppOnStoreVersion()) > getPackageManager().getPackageInfo(getPackageName(), 0).versionCode){
//			if(Integer.valueOf(2) > getPackageManager().getPackageInfo(getPackageName(), 0).versionCode){
				return true;
			}
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}


	private int checkDateVote() {
		
		Date vote_startDate,vote_endDate;
		long TimeMillSec1,TimeMillSec2,diff_before = 0,diff_after = 0;
			try {
				vote_startDate = formatter.parse(DATE_VOTE_START);
				vote_endDate = formatter.parse(DATE_VOTE_END);
				TimeMillSec1 = vote_startDate.getTime();
				TimeMillSec2 = vote_endDate.getTime();
				
				nowTimeMilliSec = System.currentTimeMillis();
	
				diff_before = TimeMillSec1 - nowTimeMilliSec; // - ok
				diff_after = TimeMillSec2 - nowTimeMilliSec; // +ok
				Log.i("", "checkdatevote : "+diff_before+"//"+diff_after);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		if(diff_before>0 && diff_after>0) return BEFORE_VOTE;
		else if (diff_after<0 && diff_before<0) return AFTER_VOTE;
		else return DURING_VOTE;
	}
	
	private int checkDateDeclare() {
		Date vote_startDate;
		long TimeMillSec,diff_before = 0;
			try {
				vote_startDate = formatter.parse(DATE_WINNER);
				TimeMillSec = vote_startDate.getTime();
				
				nowTimeMilliSec = System.currentTimeMillis();
	
				diff_before = TimeMillSec - nowTimeMilliSec; // - ok
				Log.i("", "checkdatevote : "+diff_before);
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		if(diff_before<0 ) return DECLARE;
		else return NO_DECLARE;
	}
	
	private void Getdata_List() {
		
		
		int k=1;
		for(int i=1;i<=15;++i){
			list_star l_s = new list_star();
			Log.i("", "string "+getString(getResources().getIdentifier("Title"+i, "string", getPackageName())) );
			l_s.setTitle(getString(getResources().getIdentifier("Title"+i, "string", getPackageName())));
			List<name_star> Starname = new ArrayList<name_star>();
			
			
			if(i<8) for(int j=0;j<5;j++) {
				Log.i("", "string name "+getString(getResources().getIdentifier("G"+k, "string", getPackageName())) );
				name_star name = new name_star();
				name.setName(getString(getResources().getIdentifier("G"+k, "string", getPackageName())));
				name.setNumber("G"+k);
				name.setResource_image(getResources().getIdentifier("g"+k, "drawable", getPackageName()));
				name.setResource_image_resultvote(getResources().getIdentifier("vg"+k, "drawable", getPackageName()));
				Starname.add(name);
				k++;
			}
			else if(i>7 && i<14) for(int j=1;j<=10;j++) {
				Log.i("", "string name "+getString(getResources().getIdentifier("G"+k, "string", getPackageName())) );
				name_star name = new name_star();
				name.setName(getString(getResources().getIdentifier("G"+k, "string", getPackageName())));
				name.setResource_image(getResources().getIdentifier("g"+k, "drawable", getPackageName()));
				name.setResource_image_resultvote(getResources().getIdentifier("vg"+k, "drawable", getPackageName()));
				name.setNumber("G"+k);
				Starname.add(name);
				k++;
			}
			else if(i==14) for(int j=0;j<5;j++) {
				Log.i("", "string name "+getString(getResources().getIdentifier("G"+k, "string", getPackageName())) );
				name_star name = new name_star();
				name.setName(getString(getResources().getIdentifier("G"+k, "string", getPackageName())));
				name.setResource_image(getResources().getIdentifier("g"+k, "drawable", getPackageName()));
				name.setResource_image_resultvote(getResources().getIdentifier("vg"+k, "drawable", getPackageName()));
				name.setNumber("G"+k);
				Starname.add(name);
				k++;
			}else for(int j=0;j<6;j++) {
				Log.i("", "string name "+getString(getResources().getIdentifier("G"+k, "string", getPackageName())) );
				name_star name = new name_star();
				name.setName(getString(getResources().getIdentifier("G"+k, "string", getPackageName())));
				name.setResource_image(getResources().getIdentifier("g"+k, "drawable", getPackageName()));
				name.setResource_image_resultvote(getResources().getIdentifier("vg"+k, "drawable", getPackageName()));
				name.setNumber("G"+k);
				Starname.add(name);
				k++;
			}
			l_s.setList_name(Starname);
			MyListStar.add(l_s);
		}
		
		
		
	}


	private void setDateText(final long millisec) {

		runOnUiThread(new Runnable() {
			public void run() {
				long seconds = (long) (millisec / 1000) % 60;

				long minutes = (long) ((millisec / (1000 * 60)) % 60);

				long hours = (long) ((millisec / (1000 * 60 * 60)) % 24);

				long days = (int) ((millisec / (1000 * 60 * 60 * 24)) % 365);
				
				
				 String serverUptimeText1 = String.format("%02d",days);
				 String serverUptimeText2 = String.format("%02d",hours);
				 String serverUptimeText3 = String.format("%02d",minutes);
				 String serverUptimeText4 = String.format("%02d",seconds); 
				 
				 if(millisec>0){
					 tvTime1.setText(serverUptimeText1);
					 tvTime2.setText(serverUptimeText2);
					 tvTime3.setText(serverUptimeText3);
					 tvTime4.setText(serverUptimeText4);
				}else{
					 tvTime1.setText("00");
					 tvTime2.setText("00");
					 tvTime3.setText("00");
					 tvTime4.setText("00");
				}
				
				
			}
		});

	}
	
// Parser----------------------------------------------------------------------------------
	 // you can make this class as another java file so it will be separated from your main activity.
    public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        
        // set your json string url here
        String yourJsonStringUrl = "http://thestar9.gmmwireless.com:80/thestar9/api_awards/index.php/banner/get_android";
        String Comeback;
        // contacts JSONArray
        JSONArray dataJsonArr = null;

        
        protected void onPreExecute() {}

        
        protected String doInBackground(String... arg0) {

            try {
            	/***********************************************************************/
            	//use getJsonFromURl for getData
            	Comeback = getJsonFromURL(yourJsonStringUrl);
            	Log.d(TAG, Comeback);
            	//use JSON for getstring
            	if(Comeback!=null){
            	JSONObject jObj = new JSONObject(Comeback);
            	//JSONObject catOject = jObj.getJSONObject("DATA");
            	t_data = jObj.getString("DATA");
            	t_status = jObj.getString("STATUS_DETAIL");
            	gg = "Data:"+t_data+" \n Status:"+t_status;
            	}
            	//use for substring
            	String tem = t_data.substring(2, t_data.length()-2);
            	String tem2 = tem.replace("\\/", "/");
            	gg = tem2;
            	/***********************************************************************/
                

            } catch (Exception e) {
                e.printStackTrace();
            }
            
           

            return null;
        }
        /***********************************************************************/
        //ADD this Method
        private String getJsonFromURL(String URLGET)throws IOException {
			// TODO Auto-generated method stub
        	try {
				URL url = new URL(URLGET);
				HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
				httpCon.setConnectTimeout(6*1000);
				httpCon.connect();
				int responeCode = httpCon.getResponseCode();
				if(responeCode == HttpURLConnection.HTTP_OK){
					InputStream ins = httpCon.getInputStream();
					BufferedReader rd = new BufferedReader(new InputStreamReader(ins, "UTF-8"));
					String line;
					StringBuffer respone = new StringBuffer();
					while ((line=rd.readLine())!=null )
					{
						respone.append(line);
						respone.append("\n");
					}
					rd.close();
					return respone.toString();
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
			return null;
		}
        /***********************************************************************/
		private Collection load(JSONObject json) {
			// TODO Auto-generated method stub
			return null;
		}

		
        protected void onPostExecute(String strFromDoInBg) {
        	///*tt.setText(list_voteData.get(0).getImgBan());
//        	tt.setText(gg);
        	Log.d(TAG, "onPostExe");
//        	Log.d(TAG, gg);
        	new DownloadImageTask(img).execute(gg);
//        	for(VoteData vd : list_voteData)
//        	{
//        		Log.i("","list : "+ vd.getURL());
//        	}
        }
        
        
    }
    
    
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap>{
    	ImageView bmImage;
    	
    	public DownloadImageTask(ImageView bmImage) {
    		this.bmImage = bmImage;
    	}
    	
    	protected Bitmap doInBackground(String... urls) {
    		String urldisplay = urls[0];
    		Bitmap mIcon11 = null;
    		
    		try {
    			InputStream in = new java.net.URL(urldisplay).openStream();
    			mIcon11 = BitmapFactory.decodeStream(in);
    		}catch (Exception e) {
    			Log.e("Error", e.getMessage());
    			e.printStackTrace();
    		}
    		return mIcon11;
    	}
    	protected void onPostExecute(Bitmap result) {
    		bmImage.setImageBitmap(result);
    	}
    }
	
	
	
	
//-----------------------------------------------------------------------------------------

	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

}
