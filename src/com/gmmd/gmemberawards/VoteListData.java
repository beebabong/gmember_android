package com.gmmd.gmemberawards;

public class VoteListData {
	
	String NAME;
	String PERCENTAGE;
	String ID;
	String IMAGE_URL;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getIMAGE_URL() {
		return IMAGE_URL;
	}
	public void setIMAGE_URL(String iMAGE_URL) {
		IMAGE_URL = iMAGE_URL;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getPERCENTAGE() {
		return PERCENTAGE;
	}
	public void setPERCENTAGE(String pERCENTAGE) {
		PERCENTAGE = pERCENTAGE;
	}

}
