package com.gmmd.gmemberawards;


import com.gmmd.gmemberawards.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class Dialog_Vote extends Dialog implements android.view.View.OnClickListener{

	Button btn_submit,btn_cancel;
	TextView txt_detail;
	Context c;
	String sms,message;
	
	public Dialog_Vote(Context context,String sms,String message) {
		super(context);
		this.sms = sms;
		this.message = message;
		c= context;
	}

	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 this.setContentView(R.layout.showdialog_logout); 
		 btn_submit = (Button) findViewById(R.id.btn_submit);
		 btn_cancel = (Button) findViewById(R.id.btn_cancel);
		 txt_detail = (TextView) findViewById(R.id.txt_detail);
		 
		 this.setTitle("โหวต");
		 txt_detail.setText("คุณต้องการโหวตให้กับ "+message);
		 
		 btn_submit.setOnClickListener(this);
		 btn_cancel.setOnClickListener(this);
	}

	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_submit:
			Log.i("","send SMS "+ sms + "to 4242811");
			SentSMS("4242811",sms);
//			SentSMS("0866353663",sms);
			this.dismiss();
			break;
		case R.id.btn_cancel:
			this.dismiss();
			break;
	
		}
	}

	private void ShowDialogSendAlready() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(c);
		builder.setMessage("คุณได้ทำการโหวตให้ "+ message+" เรียบร้อยแล้ว ขอบคุณค่ะ")
		       .setCancelable(true)
		       
		       .setNeutralButton("OK", new OnClickListener() {
				
				
				public void onClick(DialogInterface arg0, int arg1) {
					dismiss();
				}
			});
		 builder.show();
	}

	private void SentSMS(String phoneNo, String sms) {
		// TODO Auto-generated method stub

		try {
		@SuppressWarnings("deprecation")
		SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(phoneNo, null,sms, null, null);
		ShowDialogSendAlready();

		} catch (Exception e) {
		Toast.makeText(c,"SMS faild, please try again later!"+e.getMessage().toString(),
		Toast.LENGTH_LONG).show();
		e.printStackTrace();
		}
		}
	
}
