package com.gmmd.gmemberawards;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gmmd.gmemberawards.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class VoteResultDetailActivity extends Activity{
	
List<VoteListData> list_voteData = new ArrayList<VoteListData>();
	
	private ListView listview;
	TextView txt_subtitle;
	int position;
	String range_category;
	private MyListViewAdapter myListViewAdapter;
	
	private Context context;
	DisplayImageOptions options;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	boolean version_control = false;
	int width;
	ProgressBar progressBar1;
	@SuppressLint("NewApi")
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vote_result_detail);
		
		
		//List View
		context = this;
		listview = (ListView) findViewById(R.id.listView1);
		txt_subtitle = (TextView) findViewById(R.id.txt_subtitle);
		progressBar1 = (ProgressBar ) findViewById(R.id.progressBar1);
		position =  getIntent().getIntExtra("position", 0);
		range_category = getIntent().getStringExtra("range_category");
		 Log.i("","range_category"+range_category+"//"+DeviceInfo.isCheckInternet(this));
		
		txt_subtitle.setText(MainMenuActivity.MyListStar.get(position).getTitle().toString());
		//-------------------------------------------------

    	width = getWindowManager().getDefaultDisplay().getWidth();
		
		// we will using AsyncTask during parsing 
		if(DeviceInfo.isCheckInternet(this)) new AsyncTaskParseJson().execute();
		else {
			progressBar1.setVisibility(View.INVISIBLE);
			new Dialog_Alert(VoteResultDetailActivity.this, "Sorry, Not Internet Connection.").show();
		}
	
	}


	 // you can make this class as another java file so it will be separated from your main activity.
    public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

        final String TAG = "AsyncTaskParseJson.java";
        
        // set your json string url here
        
        String yourJsonStringUrl = "http://thestar9.gmmwireless.com:80/thestar9/api_awards/index.php/vote/getByCategoryId?CATEGORY_ID="+range_category;
       
       
        // contacts JSONArray
        JSONArray dataJsonArr = null;

        
        protected void onPreExecute() {}

        
        protected String doInBackground(String... arg0) {

            try {

                // instantiate our json parser
                JsonParser jParser = new JsonParser();

                // get json string from url
                JSONObject json = jParser.getJSONFromUrl(yourJsonStringUrl);
                
               //get status and detail
                String status = json.getString("STATUS");
                String detail = json.getString("DETAIL");
                String version = json.getString("VERSION_CONTROL");
                if(!version.equals("1")) version_control=true;
                Log.e(TAG, "STATUS: "+status);
                Log.e(TAG, "DETAIL: "+detail);
                
//                tt.setText(status);
                
                // get the array of data
                dataJsonArr = json.getJSONArray("DATA");
                

                // loop through all data
                for (int i = 0; i < dataJsonArr.length(); i++) {
                	VoteListData votelistdata = new VoteListData();
                    JSONObject datalistVote = dataJsonArr.getJSONObject(i);

                    // Storing each json item in variable
                    String ID = datalistVote.getString("ID");
                    String image = datalistVote.getString("IMAGES_URL");
                    String name = datalistVote.getString("NAME");
                    String percntage = datalistVote.getString("PERCENTAGE");

                   
                    //add data (string) to votelistdata
                    votelistdata.setNAME(name);
                    votelistdata.setPERCENTAGE(percntage);
                    votelistdata.setID(ID);
                    votelistdata.setIMAGE_URL(image);
                    
                    //add data (Object) to list_voteData
                    list_voteData.add(votelistdata);
                    
                    // show the values in our logcat
                    Log.e(TAG, "CATEGORY_ID: " + percntage 
                            + ", URL: " + name
                            );

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            
           

            return null;
        }
        
       

        
        protected void onPostExecute(String strFromDoInBg) {
//        	tt.setText(list_voteData.get(0).getNAME());
        	
        	// Show List View-----------------------------

			progressBar1.setVisibility(View.INVISIBLE);
        	myListViewAdapter = new MyListViewAdapter();
    		listview.setAdapter(myListViewAdapter);
        	
    		//--------------------------------------------
        	for(VoteListData vd : list_voteData)
        	{
        		Log.i("","list : "+ vd.getNAME());
        	}
        }
        
        
    }
    //List View----------------------------------------------------------------------
    private class MyListViewAdapter extends BaseAdapter{
		
		private ViewHolder viewHolder;

		
		public int getCount() {
			return list_voteData.size();
		}

		
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		
		public View getView(int pos, View view, ViewGroup parent) {
			
			options = new DisplayImageOptions.Builder()
			.showStubImage(MainMenuActivity.MyListStar.get(position).getList_name().get(pos).getResource_image())
			.showImageForEmptyUri(MainMenuActivity.MyListStar.get(position).getList_name().get(pos).getResource_image())
			.showImageOnFail(MainMenuActivity.MyListStar.get(position).getList_name().get(pos).getResource_image()).cacheInMemory()
			.cacheOnDisc().bitmapConfig(Bitmap.Config.RGB_565).build();
			imageLoader.init(ImageLoaderConfiguration.createDefault(VoteResultDetailActivity.this));
			        
			//Create
			if(view==null){
				view = LayoutInflater.from(context).inflate(R.layout.layout_vote_result_detail, null);
				viewHolder = new ViewHolder();
				viewHolder.txt_name = (TextView) view.findViewById(R.id.txt_name);
				viewHolder.txt_result = (TextView) view.findViewById(R.id.txt_result);
				viewHolder.img_person = (ImageView) view.findViewById(R.id.img_person);
				
				view.setTag(viewHolder);
			}else{
				viewHolder = (ViewHolder) view.getTag();
				
			}

			
			//Assign
			viewHolder.txt_name.setText(MainMenuActivity.MyListStar.get(position).getList_name().get(pos).getName());
			viewHolder.txt_result.setText(list_voteData.get(pos).getPERCENTAGE());
			LinearLayout.LayoutParams flp;
			flp= new LinearLayout.LayoutParams(width/3,width/3 );
			flp.setMargins(10, 0, 0, 0);
			viewHolder.img_person.setLayoutParams(flp);
			
			if(!version_control){
				viewHolder.img_person.setImageResource(MainMenuActivity.MyListStar.get(position).getList_name().get(pos).getResource_image_resultvote());
			}
			else imageLoader.displayImage(list_voteData.get(pos).getIMAGE_URL(), viewHolder.img_person,options);
			
			return view;
			
		}
		
		private class ViewHolder{
			
			public TextView txt_name,txt_result;
			public ImageView img_person;
		}
		
	}

  //End List View----------------------------------------------------------------------
}
